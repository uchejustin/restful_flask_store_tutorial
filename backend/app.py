# pylint: disable=wrong-import-position
"""
Sample restful api for a store
"""
from dotenv import load_dotenv

load_dotenv(".env")

from flask import Flask, jsonify
from flask_restful import Api
from flask_jwt_extended import JWTManager
from flask_uploads import configure_uploads, patch_request_class

from flask_migrate import Migrate
from werkzeug.security import safe_str_cmp
from marshmallow import ValidationError

from marshmallow_init import ma
from resources.confirmation_resource import Confirmation, ConfirmationByUser
from resources.user_resource import (
    UserResource,
    UserRegisterResource,
    UserLogin,
    TokenRefresh,
    UserLogout,
    SetUserPasswordResource,
)
from resources.item_resource import ItemResource, ItemListResource
from resources.store_resource import StoreResource, StoreListResource
from resources.image_resource import (
    ImageUploadResource,
    ImageResource,
    AvatarUploadResource,
    AvatarResource,
)
from resources.github_login_resource import (
    GithubLoginResource,
    GithubLoginAuthorize,
)
from resources.order_resource import OrderResource
from libs.image_handler import IMAGE_SET
from libs.oa import app_oauth
from models.db_handler import db
from models.user_model import UserModel
from blacklist import USERID_BLACKLIST, BLACKLIST


server = Flask(__name__)
server.config.from_object("default_config")
# overwrites same keys in default config
server.config.from_envvar("APPLICATION_CONFIG")
# limit max size of images to 10MB
patch_request_class(server, 10 * 1024 * 1024)
# activate uploads for image set
configure_uploads(server, IMAGE_SET)
server_api = Api(server)
db.init_app(server)
ma.init_app(server)
app_oauth.init_app(server)
# plugin to enable for migrations
migrate = Migrate(server, db)


@server.before_first_request
def create_tables():
    """ Function for initializing sqlalchemy db object """
    db.create_all()


@server.errorhandler(ValidationError)
def handle_marshmallow_validations(error):
    """ Function for handling marshmallow errors """
    return jsonify(error.messages), 400


jwt = JWTManager(server)


@jwt.user_claims_loader
def add_claims_to_jwt(identity):
    """ Function for add additional data to jwt """
    user = UserModel.find_by_id(identity)
    if user and safe_str_cmp("user_uche", user.username):
        # if identity == 1:  # to be changed later
        return {"is_admin": True}
    return {"is_admin": False}


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    """ Function for implementing blacklisting of users and tokens """
    return (
        decrypted_token["jti"] in BLACKLIST
        or decrypted_token["identity"] in USERID_BLACKLIST
    )


@jwt.expired_token_loader
def expired_token_callback():
    """ Function for customizing jwt expired response """
    return jsonify({"message": "The token has expired.", "error": "token_expired"}), 401


@jwt.invalid_token_loader
def invalid_token_callback(error):  # error is passed in by the caller internally
    """ Function for customizing invalid jwt response """
    return (
        jsonify(
            {
                "message": "Signature verification failed.",
                "error": "invalid_token",
                "msg": error,
            }
        ),
        401,
    )


@jwt.unauthorized_loader
def missing_token_callback(error):
    """ Function for customizing missing jwt tokens response """
    return (
        jsonify(
            {
                "description": "Request does not contain an access token.",
                "error": "authorization_required",
                "msg": error,
            }
        ),
        401,
    )


@jwt.needs_fresh_token_loader
def token_not_fresh_callback():
    """ Function for indicating if jwt token not fresh """
    return (
        jsonify(
            {"description": "The token is not fresh.", "error": "fresh_token_required"}
        ),
        401,
    )


@jwt.revoked_token_loader
def revoked_token_callback():
    """ Function for customizing jwt revoked response """
    return (
        jsonify(
            {"description": "The token has been revoked.", "error": "token_revoked"}
        ),
        401,
    )


server_api.add_resource(StoreResource, "/store/<string:name>")
server_api.add_resource(StoreListResource, "/stores")
server_api.add_resource(ItemResource, "/item/<string:name>")
server_api.add_resource(ItemListResource, "/items")
server_api.add_resource(UserRegisterResource, "/register")
server_api.add_resource(UserResource, "/user/<string:user_id>")
server_api.add_resource(UserLogin, "/login")
server_api.add_resource(TokenRefresh, "/refresh")
server_api.add_resource(UserLogout, "/logout")
server_api.add_resource(Confirmation, "/confirm/<string:confirmation_id>")
server_api.add_resource(ConfirmationByUser, "/user_confirm/<string:user_id>")
server_api.add_resource(ImageUploadResource, "/upload/image")
server_api.add_resource(ImageResource, "/image/<string:filename>")
server_api.add_resource(AvatarUploadResource, "/upload/avatar")
server_api.add_resource(AvatarResource, "/avatar/<string:user_id>")
server_api.add_resource(GithubLoginResource, "/login/github")
server_api.add_resource(
    GithubLoginAuthorize, "/login/github/authorized", endpoint="github.authorize"
)
server_api.add_resource(SetUserPasswordResource, "/login/password/reset")
server_api.add_resource(OrderResource, "/order")
