# pylint: disable=no-member
""" Module for providing a User Model: an internal representation of user entity """

from requests import Response
from flask import request, url_for
from flask_sqlalchemy import sqlalchemy
from sqlalchemy.dialects.postgresql import UUID
import uuid
from models.db_handler import db
from models.confirmation_model import ConfirmationModel
from libs.mailgun import MailgunHandler
import enum


class UserEnumType(enum.Enum):
    """
    A class to represent a user type.
    """

    design = "design"
    finance = "finance"
    adminuser = "adminuser"
    normal = "normal"


class UserModel(db.Model):
    """
    A class to represent a user entity.
    -------------------
    Required Attributes
    -------------------
    username: str
    password: str
    """

    __tablename__ = "users"
    id = db.Column(
        UUID(as_uuid=True),
        primary_key=True,
        default=uuid.uuid4,
        unique=True,
    )
    # id = db.Column(db.Integer, db.Sequence("user_id_seq"), primary_key=True)
    username = db.Column(db.String(50), nullable=False, unique=True)
    password = db.Column(db.String(128), nullable=False)
    usertype = db.Column(
        db.Enum(UserEnumType), default=UserEnumType.normal, nullable=False
    )
    email = db.Column(db.String(80), nullable=True, unique=True)
    confirmation = db.relationship(
        "ConfirmationModel", lazy="dynamic", cascade="all, delete-orphan"
    )

    @property
    def most_recent_confirmation(self) -> "ConfirmationModel":
        """ Method providing most recent confirmation token from returned query object """
        if self.confirmation:
            return self.confirmation.order_by(
                db.desc(ConfirmationModel.expires_at)
            ).first()

    def send_confirmation_email(self) -> Response:
        """ Method providing login link using user id """
        link = request.url_root[0:-1] + url_for(
            "confirmation", confirmation_id=self.most_recent_confirmation.id
        )
        subject = "Registration Confirmation"
        text = f"Please click the link to confirm your registration: {link}"
        html = f'<html>Please click the link to confirm your registration: <a href="{link}">{link}</a></html>'
        return MailgunHandler.send_email(self.email, subject, text, html)

    def save_to_db(self) -> None:
        """ Method for saving user object to db """
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self) -> None:
        """ Method for deleting user object from db """
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def find_by_username(cls, username: str) -> "UserModel":
        """ Class Method for searching for users with username """
        return cls.query.filter_by(username=username).first()

    @classmethod
    def find_by_email(cls, email: str) -> "UserModel":
        """ Class Method for searching for users with username """
        return cls.query.filter_by(email=email).first()

    @classmethod
    def find_by_id(cls, _id: str) -> "UserModel":
        """ Class Method for searching for users with id """
        try:
            return cls.query.filter_by(id=_id).first()
        except sqlalchemy.exc.SQLAlchemyError:
            return None
