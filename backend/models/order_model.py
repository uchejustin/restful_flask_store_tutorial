# pylint: disable=no-member
""" Module for providing a Item Model: an internal representation of item entity """

from typing import List
import os
import stripe
from models.db_handler import db
from flask_sqlalchemy import sqlalchemy


CURRENCY = "usd"

# Defining normal many to many relationship between items and orders
# ITEMS_TO_ORDERS = db.Table(
#    "items_to_orders",
#    db.Column("item_id", db.Integer, db.ForeignKey("items.id")),
#    db.Column("order_id", db.Integer, db.ForeignKey("orders.id")),
# )
# items would then be
# items = db.relationship("ItemModel", secondary=ITEMS_TO_ORDERS, lazy="dynamic")


class ItemsInOrderModel(db.Model):
    """
    A class to represent association relationship between item and orders in a store.
    """

    __tablename__ = "items_in_order"
    id = db.Column(db.Integer, db.Sequence("item_in_order_id_seq"), primary_key=True)
    item_id = db.Column(db.Integer, db.ForeignKey("items.id"))
    order_id = db.Column(db.Integer, db.ForeignKey("orders.id"))
    quantity = db.Column(db.Integer)

    item = db.relationship("ItemModel")
    # back-populates makes changes to one to be reflected on other in manytomany
    # back ref can be used also without need of defining below order relationship
    order = db.relationship("OrderModel", back_populates="items_in_order")


class OrderModel(db.Model):
    """
    A class to represent a order entity in a store.
    """

    __tablename__ = "orders"
    id = db.Column(db.Integer, db.Sequence("order_id_seq"), primary_key=True)
    status = db.Column(db.String(20), nullable=False)

    items_in_order = db.relationship("ItemsInOrderModel", back_populates="order")

    @property
    def description(self) -> str:
        """ Method for returning description of order for stripe dashboard """
        items_counts = [
            f"{current_iteminorder.quantity}x {current_iteminorder.item.name}"
            for current_iteminorder in self.items_in_order
        ]
        return ",".join(items_counts)

    @property
    def amount(self) -> None:
        """ Method for calculating order amount """
        return int(
            sum(
                [
                    current_iteminorder.item.price * current_iteminorder.quantity
                    for current_iteminorder in self.items_in_order
                ]
            )
            * 100
        )

    def save_to_db(self) -> None:
        """ Method for saving item object to db """
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self) -> None:
        """ Method for deleting order object from db """
        db.session.delete(self)
        db.session.commit()

    def set_status(self, new_status: str) -> None:
        """ Atomic method for setting order status """
        self.status = new_status
        self.save_to_db()

    def charge_with_stripe(self, token: str) -> stripe.Charge:
        """ Atomic method for charging orders with stripe """
        stripe.api_key = os.getenv("STRIPE_API_SECRET")
        # here amount is in cents ie 100 cents is USD 1.00
        # customer is just a test customer id
        return stripe.Charge.create(
            customer="test_customer",
            amount=self.amount,
            currency=CURRENCY,
            description=self.description,
            source=token,
        )

    @classmethod
    def find_all(cls) -> List["OrderModel"]:
        """ Class Method for retrieving all orders objects """
        return cls.query.all()

    @classmethod
    def find_by_id(cls, _id: int) -> "OrderModel":
        """ Class Method for searching for orders with id """
        try:
            return cls.query.filter_by(id=_id).first()
        except sqlalchemy.exc.SQLAlchemyError:
            return None
