# pylint: disable=no-member
""" Module for providing a Store Model: an internal representation of store entity """

from uuid import uuid4
from time import time
from typing import List
from flask_sqlalchemy import sqlalchemy
from sqlalchemy.dialects.postgresql import UUID
from models.db_handler import db

CONFIRMATION_EXPIRATION_DELTA = 1800


class ConfirmationModel(db.Model):
    """
    A class to represent a user confirmation entity.
    """

    __tablename__ = "confirmations"
    id = db.Column(db.String(50), primary_key=True)
    expires_at = db.Column(db.Integer, nullable=False)
    confirmed = db.Column(db.Boolean, nullable=False, default=False)
    user_id = db.Column(
        UUID(as_uuid=True),
        db.ForeignKey("users.id"),
    )

    user = db.relationship("UserModel")

    def __init__(self, user_id: str, **kwargs):
        """ Constructor for confimration model """
        super().__init__(**kwargs)
        self.user_id = user_id
        self.id = uuid4().hex
        self.expires_at = int(time()) + CONFIRMATION_EXPIRATION_DELTA

    @classmethod
    def find_by_id(cls, confirmation_id: str) -> "ConfirmationModel":
        """ Class Method for searching for confirmations with id """
        try:
            return cls.query.filter_by(id=confirmation_id).first()
        except sqlalchemy.exc.SQLAlchemyError:
            return None

    @property
    def is_expired(self) -> bool:
        """ Method for checking if confirmation has expired """
        return time() > self.expires_at

    def force_to_expire(self) -> None:
        """ Method to force expiration of token """
        if not self.is_expired:
            self.expires_at = int(time())
            self.save_to_db()

    def save_to_db(self) -> None:
        """ Method for saving confirmation object to db """
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self) -> None:
        """ Method for deleting confirmation object from db """
        db.session.delete(self)
        db.session.commit()
