# pylint: disable=no-member
""" Module for providing a Store Model: an internal representation of store entity """

from typing import List
from models.db_handler import db


class StoreModel(db.Model):
    """
    A class to represent a store entity.
    -------------------
    Required Attributes
    -------------------
    name: str
    """

    __tablename__ = "stores"
    id = db.Column(db.Integer, db.Sequence("store_id_seq"), primary_key=True)
    name = db.Column(db.String(50), nullable=False, unique=True)

    items = db.relationship("ItemModel", lazy="dynamic")

    def save_to_db(self) -> None:
        """ Method for saving store object to db """
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self) -> None:
        """ Method for deleting store object from db """
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def find_by_store_name(cls, name: str) -> "StoreModel":
        """ Class Method for searching of stores by name """
        return cls.query.filter_by(name=name).first()

    @classmethod
    def find_all(cls) -> List["StoreModel"]:
        """ Class Method for retrieving all stores objects """
        return cls.query.all()