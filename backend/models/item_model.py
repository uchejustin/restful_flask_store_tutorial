# pylint: disable=no-member
""" Module for providing a Item Model: an internal representation of item entity """

from typing import List
from models.db_handler import db
from flask_sqlalchemy import sqlalchemy


class ItemModel(db.Model):
    """
    A class to represent a item entity in a store.
    -------------------
    Required Attributes
    -------------------
    name: str
    price: float
    store_id: int
    """

    __tablename__ = "items"
    id = db.Column(db.Integer, db.Sequence("item_id_seq"), primary_key=True)
    name = db.Column(db.String(50), nullable=False, unique=True)
    price = db.Column(db.Float(precision=2), nullable=False)
    store_id = db.Column(db.Integer, db.ForeignKey("stores.id"), nullable=False)
    store = db.relationship("StoreModel")

    def save_to_db(self) -> None:
        """ Method for saving item object to db """
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self) -> None:
        """ Method for deleting item object from db """
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def find_by_item_name(cls, name: str) -> "ItemModel":
        """ Class Method for searching for items by name """
        return cls.query.filter_by(name=name).first()

    @classmethod
    def find_all(cls) -> List["ItemModel"]:
        """ Class Method for retrieving all item objects """
        return cls.query.all()

    @classmethod
    def find_by_id(cls, _id: str) -> "ItemModel":
        """ Class Method for searching for items with id """
        try:
            return cls.query.filter_by(id=_id).first()
        except sqlalchemy.exc.SQLAlchemyError:
            return None
