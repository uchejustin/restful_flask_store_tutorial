# pylint: disable=no-member
""" Module for providing image handling library """

import os
import re
from typing import Union
from werkzeug.datastructures import FileStorage
from flask_uploads import UploadSet, IMAGES

# the images means it by default will be found in path static/images/filename.ok_extension
# ie the name images must match with the UPLOADED_IMAGES_DEST environment variable
IMAGE_SET = UploadSet("images", IMAGES)


def save_image(image: FileStorage, folder: str = None, name: str = None) -> str:
    """ Take a FileStorage object and save it to a folder """
    return IMAGE_SET.save(image, folder, name)


def geth_path(filename: str = None, folder: str = None) -> str:
    """ Gets an image name and returns its full path """
    return IMAGE_SET.path(filename, folder)


def find_image_any_format(filename: str = None, folder: str = None) -> Union[str, None]:
    """ Takes a filename and returns image in any accepted format """
    for _format in IMAGES:
        image = f"{filename}.{_format}"
        image_path = IMAGE_SET.path(filename=image, folder=folder)
        if os.path.isfile(image_path):
            return image_path
    return None


def _retrieve_filename(file: Union[str, FileStorage]) -> str:
    """ Gets a filename from a FileStorage object """
    return file.filename if isinstance(file, FileStorage) else file


def is_filename_safe(file: Union[str, FileStorage]) -> bool:
    """ Checks with regex if string is acceptable """
    filename = _retrieve_filename(file)
    allowed_format = "|".join(IMAGES)  # ie png|svg|jpe|jpeg
    regex = f"^[a-zA-Z0-9][a-zA-Z0-9_()-\\.]*\\.({allowed_format})$"
    return re.match(regex, filename)


def get_basename(file: Union[str, FileStorage]) -> str:
    """ Extracts full name of image from a filepath """
    filename = _retrieve_filename(file)
    return os.path.split(filename)[1]


def get_extension(file: Union[str, FileStorage]) -> str:
    """ Extracts the file extension"""
    filename = _retrieve_filename(file)
    return os.path.splitext(filename)[1]
