""" Module for to provide password encryption """

from passlib.context import CryptContext
from uuid import uuid4

PWD_CONTEXT = CryptContext(
    schemes=["pbkdf2_sha256"],
    default="pbkdf2_sha256",
    pbkdf2_sha256__default_rounds=30000,
)


def encrypt_password(password):
    """ Function for encrypting passwords """
    return PWD_CONTEXT.hash(password)


def check_encrypted_password(password, hashed):
    """ Function for verifying encrypted passwords """
    return PWD_CONTEXT.verify(password, hashed)


def generate_random_password():
    """ Function for random passwords for github users """
    return uuid4().hex
