"""
Module for storing flask oauth object
"""

import os
from flask import g
from flask_oauthlib.client import OAuth

app_oauth = OAuth()


github = app_oauth.remote_app(
    "test_github",
    consumer_key=os.getenv("GITHUB_APP_KEY"),
    consumer_secret=os.getenv("GITHUB_APP_SECRET"),
    request_token_params={"scope": "user:email"},
    base_url="https://api.github.com/",
    request_token_url=None,
    access_token_method="POST",
    access_token_url="https://github.com/login/oauth/access_token",
    authorize_url="https://github.com/login/oauth/authorize",
)


@github.tokengetter
def get_github_user_token():
    """ Function for returning github user token if found in global variable g"""
    if "access_token" in g:
        return g.access_token
