# pylint: disable=no-member
""" Module for providing text content based on the locale """

import json

default_locale = "en-gb"
cached_strings = {}


def refresh():
    """ Function for providing text content """
    global cached_strings
    with open(f"conf/{default_locale}.json") as f:
        cached_strings = json.load(f)


def gettext(name):
    """ Function for retrieving a specific text """
    return cached_strings[name]


refresh()
