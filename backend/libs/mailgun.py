# pylint: disable=no-member
""" Module for providing a email sending library """

from typing import List
from os import getenv as os_getenv
from requests import Response, post
from libs.translation import gettext


class MailgunException(Exception):
    """
    A class to define mailgun user-defined exception.
    -------------------
    """

    def __init__(self, message: str):
        super().__init__(message)


class MailgunHandler:
    """
    A class to serve as library for sending emails.
    -------------------
    """

    MAILGUN_BASEURL = os_getenv("MAILGUN_BASEURL")
    MAILGUN_API_KEY = os_getenv("MAILGUN_API_KEY")
    API_EMAIL = os_getenv("API_EMAIL")
    FROM_TITLE = "Test Flask Rest API"

    @classmethod
    def send_email(
        cls, email_addr: List[str], subject: str, text: str, html: str
    ) -> Response:
        """ Class method handling email sending with mailgun"""
        if cls.MAILGUN_API_KEY is None:
            raise MailgunException(gettext("MAILGUN_APIKEY_NOTFOUND"))
        if cls.MAILGUN_BASEURL is None:
            raise MailgunException(gettext("MAILGUN_DOMAIN_ERROR"))
        apiresponse = post(
            cls.MAILGUN_BASEURL,
            auth=("api", cls.MAILGUN_API_KEY),
            data={
                "from": f"{cls.FROM_TITLE} <{cls.API_EMAIL}>",
                "to": email_addr,
                "subject": subject,
                "text": text,
                "html": html,
            },
        )
        if apiresponse.status_code not in [200, 201, 202]:
            raise MailgunException(gettext("MAILGUN_CONFIRMATION_EMAIL_SENDING_ERROR"))
        return apiresponse