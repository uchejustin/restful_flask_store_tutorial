# pylint: disable=no-member
""" Module for providing global marshmallow object to be used in app """

from flask_marshmallow import Marshmallow

ma = Marshmallow()
