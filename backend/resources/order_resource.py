""" Module for exposing external representation of a customer Order """

from typing import Tuple
from collections import Counter
from flask import request
from flask_restful import Resource
from stripe import error as stripe_error

from models.item_model import ItemModel
from models.order_model import OrderModel, ItemsInOrderModel


from flask_jwt_extended import (
    jwt_required,
    get_jwt_claims,
    get_jwt_identity,
    jwt_optional,
    fresh_jwt_required,
)
from flask_sqlalchemy import sqlalchemy
from libs.translation import gettext
from schemas.order_schema import OrderSchema

order_parser = OrderSchema()


class OrderResource(Resource):
    """
    A class to provide external representation of order entity in a store.
    """

    @classmethod
    def get(cls) -> Tuple:
        """ Class Method for retrieving all orders """
        # using an alternative way to retrieve all objects
        return order_parser.dump(OrderModel.find_all(), many=True), 200

    @classmethod
    def post(cls) -> Tuple:
        """ Class Method for sending an order to stripe """
        order_json = request.get_json()
        items = []
        item_id_quantities = Counter(order_json["item_ids"])

        # NB most_common orders it and returns tuple of id and counts
        for _id, count in item_id_quantities.most_common():
            item = ItemModel.find_by_id(_id)
            if not item:
                return {"message": gettext("ORDER_ITEM_NOT_FOUND").format(_id)}, 404
            items.append(ItemsInOrderModel(item_id=_id, quantity=count))
        order = OrderModel(items_in_order=items, status="pending")
        try:
            order.save_to_db()
            order.set_status("failed")
        except sqlalchemy.exc.SQLAlchemyError:
            return {"message": gettext("ORDER_INSERT_ERROR").format(_id)}, 500
        try:
            order.charge_with_stripe(order_json["token"])
            order.set_status("complete")
        except sqlalchemy.exc.SQLAlchemyError:
            return {"message": gettext("ORDER_INSERT_ERROR").format(_id)}, 500
        except stripe_error.APIConnectionError as err:
            # Note an email can be sent here with mailgun to inform about errors
            # also translations will be lost unless custom messages are sent here
            return err.json_body, err.http_status
        except stripe_error.APIError as err:
            return err.json_body, err.http_status
        except stripe_error.AuthenticationError as err:
            return err.json_body, err.http_status
        except stripe_error.CardError as err:
            return err.json_body, err.http_status
        except stripe_error.IdempotencyError as err:
            return err.json_body, err.http_status
        except stripe_error.InvalidRequestError as err:
            return err.json_body, err.http_status
        except stripe_error.RateLimitError as err:
            return err.json_body, err.http_status
        except stripe_error.StripeError as err:
            return err.json_body, err.http_status
        except Exception as err:
            import traceback, sys

            other_err = "".join(traceback.format_exception(*sys.exc_info()))
            return {"message": gettext("ORDER_SENDING_ERROR") + ": " + other_err}, 500
        return order_parser.dump(order)
