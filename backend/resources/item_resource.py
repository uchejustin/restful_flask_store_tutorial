""" Module for exposing external representation of Item Model """

from typing import Tuple
from flask import request
from flask_restful import Resource
from flask_jwt_extended import (
    jwt_required,
    get_jwt_claims,
    get_jwt_identity,
    jwt_optional,
    fresh_jwt_required,
)
from flask_sqlalchemy import sqlalchemy
from models.item_model import ItemModel
from schemas.item_schema import ItemSchema
from libs.translation import gettext

item_parser = ItemSchema()
item_list_schema = ItemSchema(many=True)


class ItemResource(Resource):
    """
    A class to provide external representation of item entity in a store.
    -------------------
    Methods
    -------------------
    get(name: str) -> Item: dict || Error message: dict
    post(name: str, price: float, store_id: int) -> Item: dict || Error message: dict
    put(name: str, price: float, store_id: int) -> Item: dict || Error message: dict
    delete(name: str) -> Confirmation message: dict || Error message: dict
    """

    @classmethod
    def get(cls, name: str) -> Tuple:
        """ Class Method for returning json format of found item """
        item = ItemModel.find_by_item_name(name)
        if item:
            return item_parser.dump(item), 200
        return {"message": gettext("ITEM_NOT_FOUND").format(name)}, 404

    @classmethod
    @fresh_jwt_required
    def post(cls, name: str) -> Tuple:
        """ Class Method for creating new item """
        if ItemModel.find_by_item_name(name):
            return {"message": gettext("ITEM_NAME_EXISTS").format(name)}, 400

        item_json = request.get_json()  # price, store_id
        item_json["name"] = name
        item = item_parser.load(item_json)

        try:
            item.save_to_db()
        except sqlalchemy.exc.SQLAlchemyError:
            return {"message": gettext("ITEM_INSERT_ERROR").format(name)}, 500

        return item_parser.dump(item), 201

    @classmethod
    @jwt_required
    def delete(cls, name: str) -> Tuple:
        """ Class Method for deleting an item """
        claims = get_jwt_claims()
        if not claims["is_admin"]:
            return {"message": gettext("ADMIN_REQUIRED")}, 401

        item = ItemModel.find_by_item_name(name)
        if item:
            try:
                item.delete_from_db()
                return {"message": gettext("ITEM_DELETED").format(name)}, 200
            except sqlalchemy.exc.SQLAlchemyError:
                return {"message": gettext("ITEM_DELETE_ERROR").format(name)}, 500
        return {"message": gettext("ITEM_NOT_FOUND").format(name)}, 404

    @classmethod
    @jwt_required
    def put(cls, name: str) -> Tuple:
        """ Class Method for updating an item """
        item = ItemModel.find_by_item_name(name)
        item_json = request.get_json()

        if item:
            item.price = item_json.get("price")
        else:
            item_json["name"] = name
            item = item_parser.load(item_json, partial=("store_id",))

        try:
            item.save_to_db()
        except sqlalchemy.exc.SQLAlchemyError:
            return {"message": gettext("ITEM_INSERT_ERROR").format(name)}, 500

        return item_parser.dump(item), 201


class ItemListResource(Resource):
    """
    A class to provide externally all item entities.
    -------------------
    Methods
    -------------------
    get() -> ItemsList: dict
    """

    @classmethod
    @jwt_optional
    def get(cls) -> Tuple:
        """ Class Method for returning all items available """
        user_id = get_jwt_identity()
        items = item_list_schema.dump(ItemModel.find_all())
        if user_id:
            return {"items": items}, 200
        return {
            "items": [item["name"] for item in items],
            "message": gettext("LOGIN_FOR_MORE"),
        }, 200
