""" Module for exposing external representation of User Model """

import sys
from typing import Tuple
from flask import request, make_response, render_template
from flask_restful import Resource
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    jwt_required,
    get_jwt_identity,
    fresh_jwt_required,
    get_raw_jwt,
    jwt_refresh_token_required,
)
from flask_sqlalchemy import sqlalchemy
from models.user_model import UserModel
from models.confirmation_model import ConfirmationModel
from schemas.user_schema import UserSchema
from blacklist import BLACKLIST
from libs.mailgun import MailgunException
from libs.translation import gettext
from libs.security import (
    encrypt_password,
    check_encrypted_password,
)

# from werkzeug.security import generate_password_hash, check_password_hash

HTML_HEADERS = {"Content-Type": "text/html"}

user_schema = UserSchema()


class UserRegisterResource(Resource):
    """
    A class to provide external for user entity registration.
    -------------------
    Methods
    -------------------
    post(username: str, password: str) -> Confirmation message: dict || Error message: dict
    """

    @classmethod
    def post(cls) -> Tuple:
        """ Class Method for creating new user """
        user = user_schema.load(request.get_json())

        if UserModel.find_by_username(user.username):
            return {"message": gettext("USER_NAME_EXISTS").format(user.username)}, 400

        if not user.email:
            return {
                "message": gettext("USER_EMAIL_REQUIRED").format(user.username)
            }, 400

        if UserModel.find_by_email(user.email):
            return {"message": gettext("USER_EMAIL_EXISTS").format(user.email)}, 400

        try:
            unencrypted_password = user.password
            user.password = encrypt_password(unencrypted_password)
            user.save_to_db()
            confirmation = ConfirmationModel(user.id)
            confirmation.save_to_db()
        except sqlalchemy.exc.SQLAlchemyError:
            user.delete_from_db()
            return {"message": gettext("USER_INSERT_ERROR").format(user.username)}, 500

        try:
            user.send_confirmation_email()
        except MailgunException as err:
            user.delete_from_db()
            return {"message": str(err)}, 500

        return {"message": gettext("USER_CREATED").format(user.username)}, 201


class UserResource(Resource):
    """
    A class to provide external for user entity management.
    -------------------
    Methods
    -------------------
    post(username: str, password: str) -> Confirmation message: dict || Error message: dict
    """

    @classmethod
    def get(cls, user_id: str) -> Tuple:
        """ Class Method for getting a user detail"""
        user = UserModel.find_by_id(user_id)

        if not user:
            return {"message": gettext("USER_NOT_FOUND").format(user_id)}, 404
        return user_schema.dump(user), 200

    @classmethod
    @jwt_required
    def delete(cls, user_id: str) -> Tuple:
        """ Class Method for deleting a user detail"""
        user = UserModel.find_by_id(user_id)

        if not user:
            return {"message": gettext("USER_NOT_FOUND").format(user_id)}, 400

        try:
            user.delete_from_db()
            return {"message": gettext("USER_DELETED").format(user_id)}, 200
        except sqlalchemy.exc.SQLAlchemyError:
            return {"message": gettext("USER_DELETE_ERROR").format(user_id)}, 500


class UserLogin(Resource):
    """
    A class to provide external api for user login.
    -------------------
    Methods
    -------------------
    post(username: str, password: str) -> Confirmation message: dict || Error message: dict
    """

    @classmethod
    def post(cls) -> Tuple:
        """ Class Method for logging in new user """
        user_datamodel = user_schema.load(
            request.get_json(),
            partial=(
                "email",
                "usertype",
            ),
        )
        user = UserModel.find_by_username(user_datamodel.username)
        if not user:
            return {
                "message": gettext("USER_NOT_FOUND").format(user_datamodel.username)
            }, 400

        if (
            user
            and user.password
            and check_encrypted_password(user_datamodel.password, user.password)
        ):
            confirmation = user.most_recent_confirmation
            if confirmation and confirmation.confirmed:
                access_token = create_access_token(identity=user.id, fresh=True)
                refresh_token = create_refresh_token(user.id)
                return {
                    "access_token": access_token,
                    "refresh_token": refresh_token,
                }, 200
            return {
                "message": gettext("USER_NOT_CONFIRMED").format(user_datamodel.username)
            }, 400
        return {
            "message": gettext("USER_INVALID_CREDENTIALS").format(
                user_datamodel.username
            )
        }, 401


class UserLogout(Resource):
    """
    A class to provide external api logging out users.
    -------------------
    Methods
    -------------------
    post() -> Confirmation message: dict
    """

    @classmethod
    @jwt_required
    def post(cls) -> Tuple:
        """ Class Method to log out a user"""
        jti = get_raw_jwt()["jti"]
        BLACKLIST.add(jti)
        return {"message": gettext("USER_LOGGED_OUT")}, 200


class TokenRefresh(Resource):
    """
    A class to provide external api for refreshing user tokens.
    -------------------
    Methods
    -------------------
    post() -> Confirmation message: dict
    """

    @classmethod
    @jwt_refresh_token_required
    def post(cls) -> Tuple:
        """ Class Method for refreshing a user token"""
        user_datamodel = user_schema.load(
            request.get_json(),
            partial=(
                "email",
                "usertype",
                "password",
            ),
        )
        user = UserModel.find_by_username(user_datamodel.username)

        if user:
            current_user = get_jwt_identity()
            new_token = create_access_token(identity=current_user, fresh=False)
            return {"access_token": new_token}, 200
        return {"message": gettext("USER_NOT_FOUND").format("")}, 404


class SetUserPasswordResource(Resource):
    """
    A class to provide external api for changing user passwords.
    """

    @classmethod
    @fresh_jwt_required
    def post(cls) -> Tuple:
        """ Class Method for refreshing a user token"""
        user_datamodel = user_schema.load(
            request.get_json(),
            partial=(
                "email",
                "usertype",
            ),
        )
        user = UserModel.find_by_username(user_datamodel.username)
        current_identity = get_jwt_identity()
        current_user = UserModel.find_by_id(current_identity)
        if user:
            if current_user and user.username != current_user.username:
                return {"message": gettext("USER_INVALID_CREDENTIALS").format("")}, 400

            if not current_user:
                return {"message": gettext("NOT_IMPLEMENTED_ERROR").format("")}, 400
            unencrypted_password = user_datamodel.password
            user.password = encrypt_password(unencrypted_password)
            user.save_to_db()
            return {"message": gettext("USER_PASSWORD_UPDATED").format("")}, 200
        return {"message": gettext("USER_NOT_FOUND").format("")}, 404
