""" Module for exposing external representation of Store Model """

from typing import Tuple
from flask_restful import Resource
from flask_jwt_extended import jwt_required
from flask_sqlalchemy import sqlalchemy
from models.store_model import StoreModel
from schemas.store_schema import StoreSchema
from libs.translation import gettext

store_parser = StoreSchema()
store_list_schema = StoreSchema(many=True)


class StoreResource(Resource):
    """
    A class to provide external representation of store entity.
    -------------------
    Methods
    -------------------
    get(name: str) -> Store: dict || Error message: dict
    post(name: str) -> Store: dict || Error message: dict
    delete(name: str) -> Confirmation message: dict || Error message: dict
    """

    @classmethod
    def get(cls, name: str) -> Tuple:
        """ Class Method for returning json format of found store and related items """
        store = StoreModel.find_by_store_name(name)
        if store:
            return store_parser.dump(store), 200
        return {"message": gettext("STORE_NOT_FOUND").format(name)}, 404

    @classmethod
    @jwt_required
    def post(cls, name: str) -> Tuple:
        """ Class Method for creating new store """
        if StoreModel.find_by_store_name(name):
            return {"message": gettext("STORE_NAME_EXISTS").format(name)}, 400

        store = StoreModel(name=name)  # no need for load since its just name
        try:
            store.save_to_db()
        except sqlalchemy.exc.SQLAlchemyError:
            return {"message": gettext("STORE_INSERT_ERROR").format(name)}, 500

        return store_parser.dump(store), 201

    @classmethod
    @jwt_required
    def delete(cls, name: str) -> Tuple:
        """ Class Method for deleting a store """
        store = StoreModel.find_by_store_name(name)
        if store:
            try:
                store.delete_from_db()
                return {"message": gettext("STORE_DELETED").format(name)}, 200
            except sqlalchemy.exc.SQLAlchemyError:
                return {"message": gettext("STORE_DELETE_ERROR").format(name)}, 500
        return {"message": gettext("STORE_NOT_FOUND").format(name)}, 404


class StoreListResource(Resource):
    """
    A class to provide externally all store entities.
    -------------------
    Methods
    -------------------
    get() -> StoresList: dict
    """

    @classmethod
    def get(cls) -> Tuple:
        """ Class Method for returning all stores available """
        return {"stores": store_list_schema.dump(StoreModel.find_all())}, 200
