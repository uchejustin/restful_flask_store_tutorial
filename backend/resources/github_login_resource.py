""" Module for exposing external representation of Github Oauth """

import os
import sys
from typing import Tuple
from time import time
from flask import request, g
from flask_restful import Resource
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    jwt_required,
    get_jwt_identity,
    get_raw_jwt,
    jwt_refresh_token_required,
)
from flask_sqlalchemy import sqlalchemy
from libs.translation import gettext
from libs.oa import github
from libs.security import generate_random_password
from models.user_model import UserModel
from models.confirmation_model import ConfirmationModel
from schemas.user_schema import UserSchema

HTML_HEADERS = {"Content-Type": "text/html"}
user_schema = UserSchema()


class GithubLoginResource(Resource):
    """
    A class to provide external api for using github oauth.
    """

    @classmethod
    def get(cls) -> Tuple:
        """ Method for sending user to github with app details """
        try:
            # return github.authorize(url_for("github.authorize", _external=True))
            return github.authorize(callback=os.getenv("REDIRECT_APP_OAUTH_URI"))
        except Exception:
            import traceback

            err = "".join(traceback.format_exception(*sys.exc_info()))
            return {"message": gettext("GITHUB_LOGIN_OAUTH_ERROR") + ": " + err}, 500


class GithubLoginAuthorize(Resource):
    """
    A class to provide external api to provide github with user submited details
    """

    @classmethod
    def get(cls) -> Tuple:
        """
        Method for replying to github with retrieved user information
        -------------------
        github.authorize redirects user to github to sign in adn within same request
        github makes a callback to redirect url ie this class method
        where user token is stored in g and used with tokengetter
        or github_user = github.get('user', token=g.access_token)
        This user can then be saved into apps database
        """
        github_response = github.authorized_response()
        if github_response is None or github_response.get("access_token") is None:
            return {
                "message": request.args["error"]
                + ": "
                + request.args["error_description"]
            }, 500

        g.access_token = github_response["access_token"]
        github_user = github.get("user")
        # user email can be checked from github_user.data["email"] but could be null

        github_username = github_user.data["login"]
        github_email = github_user.data["email"]
        user = user_schema.load(
            {
                "username": github_username,
                "password": generate_random_password(),
                "email": github_email,
            }
        )

        if not UserModel.find_by_username(user.username):
            try:
                user.save_to_db()
                confirmation = ConfirmationModel(user.id)
                confirmation.confirmed = True
                confirmation.save_to_db()
            except sqlalchemy.exc.SQLAlchemyError:
                user.delete_from_db()
                return {
                    "message": gettext("USER_INSERT_ERROR").format(user.username)
                }, 500

        access_token = create_access_token(identity=user.id, fresh=True)
        refresh_token = create_refresh_token(user.id)
        return {
            "access_token": access_token,
            "refresh_token": refresh_token,
        }, 200
