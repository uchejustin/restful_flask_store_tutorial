""" Module for exposing images api """

import sys
import os
from typing import Tuple
from flask import send_file, request, make_response
from flask_restful import Resource
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_uploads import UploadNotAllowed
from libs.translation import gettext
from libs import image_handler
from schemas.image_schema import ImageSchema
from models.user_model import UserModel

image_parser = ImageSchema()
HTML_HEADERS = {"Content-Type": "image/jpeg"}


class ImageUploadResource(Resource):
    """
    A class to provide external access to uploading images.
    """

    @classmethod
    @jwt_required
    def post(cls) -> Tuple:
        """ Class Method for creating new image """
        data = image_parser.load(request.files)

        user_id = get_jwt_identity()
        if user_id:
            folder = f"user_{user_id}"
            try:
                image_path = image_handler.save_image(data["image"], folder=folder)
                # here flask appends a number at end if already existing, hence we use the basename
                basename = image_handler.get_basename(image_path)
                return {"message": gettext("IMAGE_UPLOADED").format(basename)}, 201
            except UploadNotAllowed:
                extension = image_handler.get_extension(data["image"])
                return {
                    "message": gettext("IMAGE_ILLEGAL_EXTENSION").format(extension)
                }, 400
        return {
            "message": gettext("USER_NOT_FOUND").format(""),
        }, 404


class ImageResource(Resource):
    """
    A class to provide external access to retrieving and deleting images.
    """

    @classmethod
    @jwt_required
    def get(cls, filename: str) -> Tuple:
        """ Class Method for retrieving image if it exists"""

        user_id = get_jwt_identity()
        if user_id:
            folder = f"user_{user_id}"
            if not image_handler.is_filename_safe(filename):
                return {
                    "message": gettext("IMAGE_ILLEGAL_FILENAME").format(filename)
                }, 400
            try:
                return make_response(
                    send_file(image_handler.geth_path(filename, folder=folder)),
                    200,
                    HTML_HEADERS,
                )
            except FileNotFoundError:
                return {"message": gettext("IMAGE_NOT_FOUND").format(filename)}, 404
        return {
            "message": gettext("USER_NOT_FOUND").format(""),
        }, 404

    @classmethod
    @jwt_required
    def delete(cls, filename: str) -> Tuple:
        """ Class Method for deleting image if it exists"""

        user_id = get_jwt_identity()
        if user_id:
            folder = f"user_{user_id}"
            if not image_handler.is_filename_safe(filename):
                return {
                    "message": gettext("IMAGE_ILLEGAL_FILENAME").format(filename)
                }, 400
            try:
                os.remove(image_handler.geth_path(filename, folder=folder))
                return {"message": gettext("IMAGE_DELETED").format(filename)}, 200
            except FileNotFoundError:
                return {"message": gettext("IMAGE_NOT_FOUND").format(filename)}, 404
            except Exception:
                import traceback

                err = "".join(traceback.format_exception(*sys.exc_info()))
                return {
                    "message": gettext("IMAGE_DELETE_FAILED").format(filename)
                    + ": "
                    + err
                }, 500
        return {
            "message": gettext("USER_NOT_FOUND").format(""),
        }, 404


class AvatarUploadResource(Resource):
    """
    A class to provide external access to uploading user avatars.
    """

    @classmethod
    @jwt_required
    def put(cls) -> Tuple:
        """ Class Method for creating or overwriting avartars """
        data = image_parser.load(request.files)

        user_id = get_jwt_identity()
        if user_id:
            filename = f"user_{user_id}"
            folder = "avatars"
            avatar_path = image_handler.find_image_any_format(filename, folder)
            if avatar_path:
                try:
                    os.remove(avatar_path)
                except Exception:
                    import traceback

                    err = "".join(traceback.format_exception(*sys.exc_info()))
                    return {
                        "message": gettext("IMAGE_DELETE_FAILED").format(filename)
                        + ": "
                        + err
                    }, 500
            try:
                ext = image_handler.get_extension(data["image"].filename)
                avatar = filename + ext
                save_avatar_path = image_handler.save_image(
                    data["image"], folder=folder, name=avatar
                )
                basename = image_handler.get_basename(save_avatar_path)
                return {"message": gettext("IMAGE_UPLOADED").format(basename)}, 201
            except UploadNotAllowed:
                extension = image_handler.get_extension(data["image"])
                return {
                    "message": gettext("IMAGE_ILLEGAL_EXTENSION").format(extension)
                }, 400
        return {
            "message": gettext("USER_NOT_FOUND").format(""),
        }, 404


class AvatarResource(Resource):
    """
    A class to provide external access to retreiving user avatars.
    """

    @classmethod
    def get(cls, user_id: str) -> Tuple:
        """ Class Method for retreiving avatars """

        user = UserModel.find_by_id(user_id)
        if user:
            filename = f"user_{user_id}"
            folder = "avatars"
            avatar_path = image_handler.find_image_any_format(filename, folder)
            if avatar_path:
                return make_response(
                    send_file(avatar_path),
                    200,
                    HTML_HEADERS,
                )
            return {
                "message": gettext("IMAGE_NOT_FOUND").format(""),
            }, 404
        return {"message": gettext("USER_NOT_FOUND").format(user_id)}, 404
