""" Module for exposing external representation of Confirmation Model """

from typing import Tuple
from time import time
from flask import request, make_response, render_template
from flask_restful import Resource
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    jwt_required,
    get_jwt_identity,
    get_raw_jwt,
    jwt_refresh_token_required,
)
from flask_sqlalchemy import sqlalchemy
from models.confirmation_model import ConfirmationModel
from models.user_model import UserModel
from schemas.confirmation_schema import ConfirmationSchema
from libs.mailgun import MailgunException
from libs.translation import gettext

HTML_HEADERS = {"Content-Type": "text/html"}

confirmation_parser = ConfirmationSchema()
confirmation_list_schema = ConfirmationSchema(many=True)


class Confirmation(Resource):
    """
    A class to provide external api for confirming users.
    -------------------
    """

    @classmethod
    def get(cls, confirmation_id: str) -> Tuple:
        """ Class Method for confirming a user"""
        confirmation_object = ConfirmationModel.find_by_id(confirmation_id)

        if not confirmation_object:
            return make_response(
                f"<html><b>{gettext('CONFIRMATION_NOT_FOUND').format(confirmation_id)}</b></html>",
                404,
                HTML_HEADERS,
            )
        if confirmation_object.is_expired:
            return make_response(
                f"<html><b>{gettext('CONFIRMATION_EXPIRED').format(confirmation_id)}</b></html>",
                400,
                HTML_HEADERS,
            )

        if confirmation_object.confirmed:
            return make_response(
                f"<html><b>{gettext('CONFIRMATION_DONE_ALREADY').format(confirmation_object.user.username)}</b></html>",
                400,
                HTML_HEADERS,
            )

        confirmation_object.confirmed = True
        confirmation_object.save_to_db()
        # return redirect("http://localhost:50012", code=302)
        return make_response(
            render_template(
                "confirmation_page.html", email=confirmation_object.user.email
            ),
            200,
            HTML_HEADERS,
        )


class ConfirmationByUser(Resource):
    """
    A class to provide external api for testing confirmations.
    -------------------
    """

    @classmethod
    def get(cls, user_id: str) -> Tuple:
        """ Test Method for returning all confirmations for a user"""
        user = UserModel.find_by_id(user_id)

        if not user:
            return {"message": gettext("USER_NOT_FOUND").format(user_id)}, 404
        # confirmation_list_schema.dump(ConfirmationModel.order_by(ConfirmationModel.expires_at).find_all())
        return (
            {
                "current_time": int(time()),
                "confirmation": confirmation_list_schema.dump(
                    user.confirmation.order_by(ConfirmationModel.expires_at).all()
                ),
            },
            200,
        )

    @classmethod
    def post(cls, user_id: str) -> Tuple:
        """ Class Method for resending confirmation token """
        user = UserModel.find_by_id(user_id)

        if not user:
            return {"message": gettext("USER_NOT_FOUND").format(user_id)}, 404
        if not user.email:
            return {"message": gettext("USER_EMAIL_REQUIRED")}
        try:
            confirmation_object = user.most_recent_confirmation
            if confirmation_object and confirmation_object.confirmed:
                return {
                    "message": gettext("CONFIRMATION_DONE_ALREADY").format(
                        confirmation_object.user.username
                    )
                }
            elif confirmation_object:
                confirmation_object.force_to_expire()
            new_confirmation_object = ConfirmationModel(user_id)
            new_confirmation_object.save_to_db()
            user.send_confirmation_email()
            return {"message": gettext("CONFIRMATION_RESEND_SUCCESSFUL")}, 201
        except MailgunException as err:
            return {"message": str(err)}, 500
        except sqlalchemy.exc.SQLAlchemyError:
            return {
                "message": gettext("CONFIRMATION_TOKEN_INSERT_ERROR").format(
                    new_confirmation_object.id
                )
            }, 500
