# pylint: skip-file
import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Sequence, create_engine, Column, Integer, String
from sqlalchemy.orm import sessionmaker


class User(Base):
    __tablename__ = "users"
    id = Column(Integer, Sequence("user_id_seq"), primary_key=True)
    username = Column(String(50))
    password = Column(String(50))

    def __repr__(self):
        return "<User(username='%s', password='%s')>" % (self.username, self.password)


def create_initial_user():
    db_uri = "postgresql://postgresuser:postgrespassword@postgres_ip_addr:postgresport/app_database"
    engine = create_engine(db_uri, echo=True)
    Base = declarative_base()
    Base.metadata.create_all(engine)
    uc_user = User(username="someusername", password="somepassword")
    Session = sessionmaker(bind=engine)
    session = Session()
    session.add(uc_user)
    session.commit()