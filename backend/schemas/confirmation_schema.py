# pylint: disable=no-member
""" Module for providing a confirmation schema: a marshmallow serializer """

from marshmallow_init import ma
from models.confirmation_model import ConfirmationModel
from models.user_model import UserModel


class ConfirmationSchema(ma.SQLAlchemyAutoSchema):
    """
    A class to represent a confirmation schema for parsing a confirmation object.
    """

    class Meta:
        """
        A class to specify which fields are loaded.
        """

        model = ConfirmationModel
        load_only = ("user",)
        dump_only = ("id", "expires_at", "confirmed")
        include_fk = True
        # NB: we dont need to load instance because we are using init method
        load_instance = True
