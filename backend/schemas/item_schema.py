# pylint: disable=no-member
""" Module for providing a item schema: a marshmallow serializer for item class """

from marshmallow_init import ma
from models.item_model import ItemModel
from models.store_model import StoreModel


class ItemSchema(ma.SQLAlchemyAutoSchema):
    """
    A class to represent a item schema for parsing a item object.
    """

    class Meta:
        """
        A class to specify which fields are loaded.
        """

        model = ItemModel
        load_only = ("store",)
        dump_only = ("id",)
        include_fk = True
        load_instance = True
