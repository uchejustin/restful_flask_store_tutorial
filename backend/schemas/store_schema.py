# pylint: disable=no-member
""" Module for providing a store schema: a marshmallow serializer for store class """

from marshmallow_init import ma
from models.store_model import StoreModel
from models.item_model import ItemModel
from schemas.item_schema import ItemSchema


class StoreSchema(ma.SQLAlchemyAutoSchema):
    """
    A class to represent a store schema for parsing a store object.
    """

    # this infers the relationship and no need for load_only = ("items",)
    items = ma.Nested(ItemSchema, many=True)

    class Meta:
        """
        A class to specify which fields are loaded.
        """

        model = StoreModel
        dump_only = ("id",)
        include_fk = True
        load_instance = True
