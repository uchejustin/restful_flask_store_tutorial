""" Marshmallow parser to de-serialize incoming image data """

from marshmallow import Schema, fields
from werkzeug.datastructures import FileStorage


class FileStorageField(fields.Field):
    """
    A class to create custom deserialization field.
    """

    default_error_messages = {"invalid": "Not a valid image"}

    def _deserialize(self, value, attr, data, **kwargs) -> FileStorage:
        """ A custom field deserialize method """
        if value is None:
            return None
        if not isinstance(value, FileStorage):
            self.fail("invalid")
        return value


class ImageSchema(Schema):
    """
    A class to carry out validation on images.
    """

    image = FileStorageField(required=True)
