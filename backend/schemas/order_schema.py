""" Marshmallow parser to parse order data """

from marshmallow_init import ma
from models.order_model import OrderModel
from models.item_model import ItemModel


class OrderSchema(ma.SQLAlchemyAutoSchema):
    """
    A class to represent a order schema for parsing a order object.
    """

    class Meta:
        """
        A class to specify which fields are loaded.
        """

        model = OrderModel
        load_only = ("token",)
        dump_only = (
            "id",
            "status",
        )
        include_fk = True
        load_instance = True
