# pylint: disable=no-member
""" Module for providing a user schema: a marshmallow serializer for user class """

from marshmallow import pre_dump
from marshmallow_init import ma
from models.user_model import UserModel
from models.confirmation_model import ConfirmationModel
from schemas.confirmation_schema import ConfirmationSchema


class UserSchema(ma.SQLAlchemyAutoSchema):
    """
    A class to represent a user schema for parsing a user object.
    """

    confirmation = ma.Nested(ConfirmationSchema, many=True)

    class Meta:
        """
        A class to specify which fields are load data.
        """

        model = UserModel
        load_only = ("password", "usertype")
        dump_only = (
            "id",
            "confirmation",
        )
        load_instance = True

    @pre_dump
    def _pre_dump(self, user: UserModel, **kwargs):
        """ Method providing most recent confirmation token in json object """
        user.confirmation = [user.most_recent_confirmation]
        return user