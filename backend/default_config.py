""" Default development flask config file """
from os import getenv as os_getenv
import os

PROPAGATE_EXCEPTIONS = True
SQLALCHEMY_DATABASE_URI = os_getenv("POSTGRES_URI")
SQLALCHEMY_TRACK_MODIFICATIONS = False
JWT_SECRET_KEY = os_getenv("JWT_SECRET_KEY")
SECRET_KEY = os_getenv("SECRET_KEY")
JWT_BLACKLIST_ENABLED = True
JWT_BLACKLIST_TOKEN_CHECKS = ["access", "refresh"]
UPLOADED_IMAGES_DEST = os.path.join("static", "images")
GITHUB_APP_KEY = os_getenv("GITHUB_APP_KEY")
GITHUB_APP_SECRET = os_getenv("GITHUB_APP_SECRET")
